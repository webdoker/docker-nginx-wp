# Docker bundle-compose, nginx, mysql, php-fpm, wordpress


Go to the directory with your project
```
cd /youprojectdirectory
git clone https://gitlab.com/webdoker/docker-nginx-wp.git .
```
change the passwords in .env and change the url server_name in nginx-conf.d/default.conf
## Run docker-compose
```
docker-compose up -d 
```
luck))